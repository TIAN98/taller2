#include "xc.h"
#include <stdio.h>
#include <stdlib.h>
#define FCY 5000000
#include <libpic30.h>
#include "config.h"
//#include "Sensores.h"
#include "Sensor_ECG.h"
#include "Sensor_SPO2.h"
#include "Sensor_Temp.h"
#include "visualizacion.h"
// Variables globales
//float v_ECG;
//float v_SPO2;
//int   v_Temp;

int main(){
    //PUERTOS ECG
    TRISBbits.TRISB14 = 1; //1 ENTRADA ANALOGICA
    //PUERTOS SPO2
    TRISBbits.TRISB2  =  1; //1 ENTRADA ANALOGICA SDA
    TRISBbits.TRISB3  =  1; //1 ENTRADA ANALOGICA SCL
    //PUERTOS TEMP
    TRISBbits.TRISB15 =  1; //1 ENTRADA ANALOGICA
    //
    TRISBbits.TRISB8 =  0; //1 SALIDA ANALOGICA SDA
    TRISBbits.TRISB9 =  0; //1 SALIDA ANALOGICA SCL
    while(1){ //Declaracion de ciclo infinito
        float A,B,C,D; 
        A= lectura_ECG(1.1);
        B =lectura_SPO2(1.1);
        C =lectura_Temp(1.1);    
        D =visualizacionP(1.1);
        return 0;
    }
    
}